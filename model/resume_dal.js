var mysql   = require('mysql');
var db  = require('./db_connection.js');
var async = require('async');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view resume_view as
 select s.*, a.street, a.zip_code from resume s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'select * from resume r left join account a on r.account_id=a.account_id order by first_name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(resume_id, callback) {
    var query = 'SELECT * FROM resume a WHERE a.resume_id = ?';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.getData = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, done) {

    var query, queryData, resumeID;

    async.waterfall([
        function(cb) {
            query = 'INSERT INTO resume (account_id, resume_name) VALUES (?, ?)';
            queryData = [params.account_id, params.resume_name];
            connection.query(query, queryData, function(err, result) {
                if (!err) {
                    resumeID = result.insertId;
                    console.log("resume finished");
                    cb(null, result);
                } else {
                    cb(err);
                }
            })
        },
        function(resume, cb) {
            query = 'INSERT INTO resume_skill (skill_id, resume_id) VALUES (?, ?)';
            queryData = [params.skill_id, resumeID];
            connection.query(query, queryData, function(err, result) {
                if (!err) {
                    console.log("resume_skill finished");
                    cb(null, resume, result);
                } else {
                    cb(err);
                }
            })
        },
        function(resume, resume_skill, cb) {
            query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?, ?)';
            queryData = [resumeID, params.company_id];
            connection.query(query, queryData, function(err, result) {
                if (!err) {
                    console.log("resume_company finished");
                    cb(null, resume, resume_skill, result);
                } else {
                    cb(err);
                }
            })

        },
        function(resume, resume_skill, resume_company, cb) {
            query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?, ?)';
            queryData = [resumeID, params.school_id];
            connection.query(query, queryData, function(err, result) {
                if (!err) {
                    console.log("resume_school finished");
                    cb(null, resumeID);
                } else {
                    cb(err);
                }
            })

        }

    ],function(err, result) {
        if (err) {
            done(err);
        } else {
            done(err, result);
        }
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    async.waterfall([
        function(callback) {
            var query = 'UPDATE resume SET resume_name = ?, account_id = ? WHERE resume_id = ?';
            var queryData = [params.resume_name, params.account_id, params.resume_id];
            console.log(queryData);
            connection.query(query, queryData, function(err, result) {
                callback(err, result)
            });
        },
        function (result, callback) {
            var query = 'UPDATE resume_skill SET skill_id = ?, resume_id = ? WHERE resume_id = ?';
            var queryData = [params.skill_id, params.resume_id, params.resume_id];
            console.log(queryData);
            connection.query(query, queryData, function(err, result) {
                callback(err, result)
            });
        },
        function (result, callback) {
            var query = 'UPDATE resume_company SET company_id = ?, resume_id = ? WHERE resume_id = ?';
            var queryData = [params.company_id, params.resume_id, params.resume_id];
            console.log(queryData);
            connection.query(query, queryData, function(err, result) {
                callback(err, result)
            });
        },
        function (result, callback) {
            var query = 'UPDATE resume_school SET school_id = ?, resume_id = ? WHERE resume_id = ?';
            var queryData = [params.school_id, params.resume_id, params.resume_id];
            console.log(queryData);
            connection.query(query, queryData, function(err, result) {
                callback(err, result)
            });
        }
    ], function (err, result) {
        callback(err, result);

    })

};

exports.edit = function(params, callback) {
    var query = 'CALL resume_getinfo ( ? )';
    var queryData = [params];

    connection.query(query, queryData, function(err, result) {
        console.log(result);
        callback(err, result);
    });
};
