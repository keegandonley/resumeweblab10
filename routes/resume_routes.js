var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');



// View All resumes
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            console.log("NEEDED:", result);
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });
});

// View the resume for the given id
router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('resume/resumeViewById', {'result': result});
           }
        });
    }
});

// Return the add a new resume form
router.get('/add/selectUser', function(req, res){
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {'accounts': result});
        }
    });
});

router.get('/add', function(req, res){
    res.render('resume/addResume');
});

// View the resume for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(!(req.query.account_id)) {
        res.send('All fields must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.getData(req.query.account_id, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                res.render('resume/addResume', {account_id: req.query.account_id, data: result});
            }
        });
    }
});

router.post('/create', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.insert(req.body, function(err, resume_id) {
        if (err) {
            console.log(err)
            res.send(err);
        }
        else {
            resume_dal.edit(resume_id, function(err, result){
                resume_dal.getData(req.body.account_id, function (err, result2) {
                    console.log(result);
                    console.log("----------");
                    console.log(result2);
                    res.render('resume/resumeUpdate', {account_id: req.body.account_id, resume_id: resume_id, resume_name: result[0][0].resume_name, company: result[1], school: result[2], skill: result[3], success: true, data: result2});
                });

            });
        }
    });
});

router.get('/edit', function(req, res){
    console.log(req.query.account_id, req.query.resume_id);
    if(req.query.account_id == null || req.query.resume_id == null) {
        res.send('Requires more data');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            resume_dal.getData(req.query.account_id, function (err, result2) {
                console.log(result);
                console.log("----------");
                console.log(result2);
                res.render('resume/resumeUpdate', {account_id: req.query.account_id, resume_id: req.query.resume_id, resume_name: result[0][0].resume_name, company: result[1], school: result[2], skill: result[3], success: false, data: result2});
            });

        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.resume_id == null) {
       res.send('A resume id is required');
   }
   else {
       resume_dal.getById(req.query.resume_id, function(err, resume){
           address_dal.getAll(function(err, address) {
               res.render('resume/resumeUpdate', {resume: resume[0], address: address});
           });
       });
   }

});

router.post('/update', function(req, res) {
    resume_dal.update(req.body, function(err, result){
       res.redirect(302, '/resume/all');
    });
});

// Delete a resume for the given resume_id
router.get('/delete', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
         resume_dal.delete(req.query.resume_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/resume/all');
             }
         });
    }
});

module.exports = router;
